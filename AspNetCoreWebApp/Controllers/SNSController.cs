﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;

namespace AspNetCoreWebApp.Controllers
{
    [Route("api/sns")]
    [ApiController]
    public class SNSController : ControllerBase
    {
        private readonly ILogger<SNSController> _logger;

        public SNSController(ILogger<SNSController> logger)
        {
            _logger = logger;
        }

        [HttpPost]
        public String SNSSubscriptionPost(String id = "")
        {
            try
            {
                // Stream req = Request.Body;
                // req.Seek(0, System.IO.SeekOrigin.Begin);
                String json = new StreamReader(Request.Body).ReadToEndAsync().Result;
                var sm = Amazon.SimpleNotificationService.Util.Message.ParseMessage(json);
                if (sm.Type.Equals("SubscriptionConfirmation")) //for confirmation
                {
                    Console.WriteLine("Subscription Message received: ");

                    _logger.LogInformation("Subscription Message received: ");

                    //logger.Info("Received Confirm subscription request");
                    if (!string.IsNullOrEmpty(sm.SubscribeURL))
                    {
                        var uri = new Uri(sm.SubscribeURL);
                        //logger.Info("uri:" + uri.ToString());
                        var baseUrl = uri.GetLeftPart(System.UriPartial.Authority);
                        var resource = sm.SubscribeURL.Replace(baseUrl, "");
                        var response = new RestClient
                        {
                            BaseUrl = new Uri(baseUrl),
                        }.Execute(new RestRequest
                        {
                            Resource = resource,
                            Method = Method.GET,
                            RequestFormat = RestSharp.DataFormat.Xml
                        });
                    }
                }
                else // For processing of messages
                {
                    //logger.Info("Message received from SNS:" + sm.TopicArn);

                    //dynamic message = JsonConvert.DeserializeObject(sm.MessageText);
                    var message = sm.MessageText;

                    Console.WriteLine("Message received: " + message);

                    _logger.LogInformation("SNS Message received: " + message);

                    //logger.Info("EventTime : " + message.detail.eventTime);
                    //logger.Info("EventName : " + message.detail.eventName);
                    //logger.Info("RequestParams : " + message.detail.requestParameters);
                    //logger.Info("ResponseParams : " + message.detail.responseElements);
                    //logger.Info("RequestID : " + message.detail.requestID);
                }
                //do stuff
                return "Success";
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);

                _logger.LogInformation("Exception: " + ex.Message);

                //logger.Info("failed");
                return "";
            }
        }
    }
}
